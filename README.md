# jii_comic_mobile

Online Manga-reading built with Flutter

---

## Notes:
### Repo chính bên GitHub 
- [Jii Comic Mobile](https://github.com/Jii-comic/jii-comic-mobile)
- [Jii Comic API](https://github.com/Jii-comic/jii-comic-api)

### Bảng quản lí dự án cũng như danh sách công việc và phân công: 
https://trello.com/invite/b/QPNXDGFs/dad1c190c29d944a288d5530c943bc13/jii-comic

### UI-Design:
https://www.figma.com/file/M9cezSh5fFQXR7O4L6eVGm/Jii-comic?node-id=62%3A341

### Hướng dẫn cấu hình API:
- File lib/utils/api_constants.dart chứa toàn bộ thông tin của API như API_HOST, API_KEY và các route cần thiết được sử dụng trong ứng dụng. 
- Các file /lib/services chứa các service tương tác chính với API thông qua request HTTP. Thường thì các màn hình sẽ gọi các service này thông qua provider cũng như điều khiển thông qua các provider tương ứng
